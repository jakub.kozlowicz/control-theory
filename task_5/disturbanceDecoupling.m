%% Disturbance Decoupling
% clear all;
close all;
syms a0 a1 a2 a3 b0 b1 b2 b3 e3 e2 e1 e0 s

%% Mathematical modelling
disp('system matrices')

A=[0,0,0,-a0; ...
   1,0,0,-a1; ...
   0,1,0 -a2; ...
   0,0,1,-a3];
B=[b0; ...
   b1; ...
   b2; ...
   b3];
E=[e0; ...
   e1; ...
   e2; ...
   e3];

C=[0,0,0,1];

pA=poly([-0.5, -1, -1.5, -2]); pA(1)=[];
pB=poly([-3, -5]); pB=[zeros(1:4-length(pB)), pB];
pE=poly(5); pE=[zeros(1:4-length(pE)), pE];

An=double(subs(A,[a3, a2, a1, a0], pA));
Bn=double(subs(B,[b3, b2, b1, b0], pB));
En=double(subs(E,[e3, e2, e1, e0], pE));

%% Controller design

% determination of relative degrees (a necessary condition
% for the controller existence)
disp('relative degrees')

rho=1; % the relative degree of the system
cA=C;

while cA*Bn == 0
    cA=cA*An;
    rho=rho+1;
end
sprintf('--> rho = %d\n',rho)

cA=C;
sigm=1;
while cA*En == 0
    cA=cA*An;
    sigm=sigm+1;
end

sprintf('--> sigma = %d\n',sigm)

if rho < sigm

% determination of the values for the parameters F and G in the control law
% u=F*x+G*v

    F=-double(inv(C*An^(rho-1)*Bn)*C*An^rho);
    G= double(inv(C*An^(rho-1)*Bn));

    r=@(t)sign(sin(0.1*t))*10; % reference signal
    d=@(t)100*cos(0.5*t);      % disturbance

%% Simulations

    x0=[1,1,1,1]';
    tf=200;

    options = odeset('RelTol',1e-10,'AbsTol',1e-12);

    close all

% the case without a disturbance decoupling controller
    M1=@(t,x)An*x + Bn*r(t)+En*d(t);
    [t,x]=ode45(M1,[0,tf],x0, options);
    y=x*C';

    figure(1);
    plot(t,y);
    xlabel('t'), ylabel('u'), title('open loop system: output');
    figure(2);
    plot(t,x);
    xlabel('t'), ylabel('x'), title('open loop system: state');

 % the case with a disturbance decoupling controller

    closedLoopEigenValues=[-10,-10];
    p=poly(closedLoopEigenValues);
    Acl=An+Bn*F+Bn*G*(-p(end)*C-p(end-1)*C*An);
    Bcl=Bn*G*p(end);

    M2=@(t,x)Acl*x + Bcl*r(t)+En*d(t);

    [t,x]=ode45(M2,[0,tf],x0, options);
    y=x*C';
    figure(3);
    plot(t,y);
    xlabel('t'), ylabel('u'), title('closed loop system: output');
    figure(4);
    plot(t,x);
    xlabel('t'), ylabel('x'), title('closed loop system: state');
else
    sprintf("--> for this system the disturbance decoupling is not feasible\n");
end

% TASK
% Carry out this script for the following cases:
% pA=poly([-0.5, -1, -1.5, -2]); - stable system
% pA=poly([0.5, 1, -1.5, -2]); - unstable system
% pB=poly([-3, -5]); - minimum phase system
% pB=poly([ 3, -5]); - nonminimum phase system
% pE=poly([5]);
% pE=poly([-5]);
% closedLoopEigenValues=[-1,-1]
% closedLoopEigenValues=[-10,-10]
% 1) Figure 1 and Figure 3: outline how the disturbance decoupling
% control law contributes to system behaviour (reference trajectory tracking)
%
% Enusres that the system track reference trajectory even for unstable systems.

% 2) Figure 2 and Figure 4 (the zero dynamics): does the disturbance
% decoupling control law ensure the input-output stability when the system
% is minimum phase?
% Can the same conclusion be drawn when the system is non-minimum phase?
%
% In minimum phase the system is stable. In non-minimum phase the
% disturbance decoupling control law does not ensure the input-output
% stability.

% 3) Figure 3: what is the influence of the values in closedLoopEigenValues
% to the settling time?
%
% The higher values of th eigenvalues the faster the system settles.

% 4) what is the relationship between the eigenvalues of Acl,
% the roots of pB and the roots of p?
%
% The roots of the both polynomials are the eigenvalues of the Acl matrix.