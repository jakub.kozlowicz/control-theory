%% A.4 satellite DYNAMICS
clear all
close all

%% A.4.2 Mathematical modeling

% definition of the satellite parameters
T=24*60*60;           % s, period of the earth turnover
m=2;                  % kg
k=4e14;               % m^3/s^2
omega=2*pi/T;         % rad/s
rR=(k/omega^2)^(1/3); % m, the radius of the satellite's orbit

% definition of a common input signals used during simulation study
% of the satellite dynamics

ur=@(t)0;
uth=@(t)0;
dr=@(t)0;
dth=@(t)0;

% definition of the satellite Dynamics (sD)
% x'=f(x,u)
% derivation of f:

sD=@(t,x) ...
    [
    x(2,:);
    -k./(x(1,:).^2)+ x(1,:).*(omega+x(4,:)).^2 + 1.0./m.*(ur(t)+dr(t));
    x(4,:);
    (-2.*x(2,:).*(omega+x(4,:)))./x(1,:)+ 1.0./(m.*x(1,:).^2).*(uth(t)+dth(t))
    ];

%% A.4.3 Equilibrium Analysis
% verification of selected solutions of the equation of satellite dynamics
x0=[rR;0;0;0];
test1=sD(0,x0);


%% A.4.4 Linearization
% computation of parameters A, B, C of an linear approximation of
% x'=f(x,u)
% y =h(x,u)
% at the equilibrium

A= ...
    [
    0,             1, 0,          0;
    3*omega^2,     0, 0, 2*omega*rR;
    0,             0, 0,          1;
    0, -(2*omega)/rR, 0,          0
    ];

B= ...
    [
    0,0,0,0;
    1/m,0,1/m,0;
    0,0,0,0;
    0,1/(m*rR^2),0,1/(m*rR^2)
    ];

% With respect to r and theta
C= [1,0,0,0;
    0,0,1,0];
% With respect to theta
%C= [0,0,1,0];

% Linearized satellite dynamics (sLD)
% Linear approximation of f(x,u) by fl(x,u):=A*x + B*u

sLD=@(t,x)A*x + B*[ur(t);uth(t);dr(t);dth(t)];

%% A.4.5 Analysis of the model
% stability of the linearized system
ev=eig(A);

figure('Name','Eigenvalues of A');
plot(real(ev), imag(ev), 'ro');
hold on;
grid on;
title('Eigenvalues of A on Complex Plane');
xlabel('Real Axis');
ylabel('Imaginary Axis');
hold off;


%% A.4.6 Simulation

% 0. System at the equilibrium point
x0=[rR;0;0;0];
x0L=x0-[rR;0;0;0];

computeAndPlot("System at the equilibrium point", sD, sLD, x0, x0L, T);

% 1. Initial disturbance for r(0)

% The initial conditions and the radial disturbance
x0=[rR*1.01;0;0;0];
x0L=x0-[rR;0;0;0];

computeAndPlot("Initial disturbance for r(0)", sD, sLD, x0, x0L, T);

% 1. Initial disturbance for phi(0)

x0=[rR;2*pi/100;0;0];
x0L=x0-[rR;0;0;0];

computeAndPlot("Initial disturbance for phi(0)", sD, sLD, x0, x0L, T);

% 2. Radial step disturbance input equal
% to 5% of the gravitational pull of the earth

% % The initial conditions and the radial disturbance
x0=[rR;0;0;0];
x0L=x0-[rR;0;0;0];
dr=@(t)(sign(t)+1)*m*9.81/2*0.05;

% Update nonlinear model
sD=@(t,x) ...
    [
    x(2,:);
    -k./(x(1,:).^2)+ x(1,:).*(omega+x(4,:)).^2 + 1.0./m.*(ur(t)+dr(t));
    x(4,:);
    (-2.*x(2,:).*(omega+x(4,:)))./x(1,:)+ 1.0./(m.*x(1,:).^2).*(uth(t)+dth(t))
    ];

% Update linear model
sLD=@(t,x)A*x + B*[ur(t);uth(t);dr(t);dth(t)];

computeAndPlot("Radial step disturbance", sD, sLD, x0, x0L, T);

% 3. Radial periodic disturbance input with the amplitude equal to 5% of
% the gravitational pull of the earth with period equal to the period of
% revolution of the earth

% The initial conditions and the radial disturbance
x0=[rR;0;0;0];
x0L=x0-[rR;0;0;0];
dr=@(t)cos(2*pi/T*t)*m*9.81*0.05;

% Update nonlinear model
sD=@(t,x) ...
    [
    x(2,:);
    -k./(x(1,:).^2)+ x(1,:).*(omega+x(4,:)).^2 + 1.0./m.*(ur(t)+dr(t));
    x(4,:);
    (-2.*x(2,:).*(omega+x(4,:)))./x(1,:)+ 1.0./(m.*x(1,:).^2).*(uth(t)+dth(t))
    ];

% Update linear model
sLD=@(t,x)A*x + B*[ur(t);uth(t);dr(t);dth(t)];

computeAndPlot("Radial periodic disturbance", sD, sLD, x0, x0L, T);