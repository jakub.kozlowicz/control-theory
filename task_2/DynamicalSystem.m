classdef DynamicalSystem
    % DYNAMICALSYSTEM is a class that represents
    % a linear, continuous time, stationary, SISO,
    % input/state/output dynamical system
  
    properties 
        name = "noname";
        A = [0];
        B = [0];
        C = [0];
        D = [0];
        u = @(t)0
    end
    properties (Dependent=true)
        M
    end
	  
    methods
        function obj=DynamicalSystem(val0, val1, val2, val3, val4, val5)
          % DYNAMICALSYSTEM is a parametric constructor.
          % It allows to construct an object of this class
          % whose properties are assigned the following values:
          % name=val0, A=val1, B=val2, C=val3, D=val4, u=val5
            if nargin == 6
                obj.name = val0;
                obj.A = val1;
                obj.B = val2;
                obj.C = val3;
                obj.D = val4;
                obj.u = val5;
            end
        end
        function m=get.M(obj)
            % This function is closely linked to the dependent property M.
            % Its body includes one step input/state maping of the
            % dynamical system represented by this class. The variable m is
            % a function handle to this mapping.
            m = @(t,x)obj.A*x+obj.B*obj.u(t);
        end
        function [x,y,u,t]=trajectory(obj, tf, x0)
            % TRAJECTORY returns the state, the output and the input
            % values stored in x, y, u that correspond to the time instants
            % stored in t ranging from 0 to tf for the initial condition x0
            [t, x] = ode45(obj.M, [0, tf], x0);
            y = (obj.C * x' + obj.D * obj.u(t'))';
            u = obj.u(t');
        end
        function dsplots(obj,tf,x0)
            % DSPLOTS plots state x, output y and input u
            % time functions over the time interval [0,tf]
            % for the initial condition x0
            [x, y, u, t] = obj.trajectory(tf, x0);   
            subplot(3,1,1); 
            plot(t, y), grid on, hold on, xlabel('t'), ylabel('y'), title(obj.name);
            subplot(3,1,2);
            plot(t, x), grid on, hold on, xlabel('t'), ylabel('x'), title(obj.name);
            subplot(3,1,3);
            plot(t, u), grid on, hold on, xlabel('t'), ylabel('u'), title(obj.name);
        end
        function dsplotsM(obj,tf,x0)
            % DSPLOTS plots state x, output y and input u (matrix)
            % time functions over the time interval [0,tf]
            % for the initial condition x0
            [x, y, u, t] = obj.trajectory(tf, x0);   
            subplot(4,1,1); 
            plot(t, y), grid on, xlabel('t'), ylabel('y'), title(obj.name);
            subplot(4,1,2);
            plot(t, x), grid on, xlabel('t'), ylabel('x'), title(obj.name);
            subplot(4,1,3);
            plot(t, u(1, :)), grid on, xlabel('t'), ylabel('u'), title(obj.name);
            subplot(4,1,4);
            plot(t, u(2, :)), grid on, xlabel('t'), ylabel('Ta'), title(obj.name);
        end
    end
end
