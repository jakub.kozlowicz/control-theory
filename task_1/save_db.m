function save_db(filename)
% SAVE_DB Saves content of the global database to the given file.
    global database;
    [fid, ~] = fopen(filename, "w");
    db_length = length(database);
    for counter = 1:db_length
        fprintf(fid, "%s\n", database(counter).name);
        fprintf(fid, "%s\n", mat2str(database(counter).A));
        fprintf(fid, "%s\n", mat2str(database(counter).B));
        fprintf(fid, "%s\n", mat2str(database(counter).C));
        fprintf(fid, "%s\n", mat2str(database(counter).D));
    end
    fclose(fid);
end