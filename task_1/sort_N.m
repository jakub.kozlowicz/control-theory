function sort_N(matrix)
% SORT_N Prints sorted list of dynamical systems from the databse by the
% specified matrix name.
    global database;
    db_size = length(database);
    norms = zeros(1, db_size);
    for counter = 1:db_size
        norms(counter) = norm(database(counter).(matrix), 2);
    end

    [~, I] = sort(norms);

    more(5);
    for counter = 1:db_size
        name = database(I(counter)).name;
        fprintf("%s\n", name);
    end
    more off;
end