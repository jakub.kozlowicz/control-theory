%% Pole Placement Control

clear all
close all

%% (A,B) pairs for subsequent numerical studies
m=2;                       % kg, the sattellite mass
omega=2*pi/(24*60*60);     % rad/s
R=(4e14/(omega^2))^(1/3);  % m, the radius of the satellite's orbit

A1= ...
[        0,             1, 0,          0; ...
 3*omega^2,             0, 0,  2*omega*R; ...
         0,             0, 0,          1; ...
         0, -(2*omega)/R , 0,          0 ...
];

B1= ...
[ ...
  0,          0; ...
1/m,          0; ...
  0,          0; ...
  0, 1/(m*R^2); ...
];

M=100;  % kg      the cart mass
M1=10;  % kg      the first link mass
M2=10;  % kg      the second link mass
L1=2;   % m       the length of the first link
L2=1;   % m       the length of the second link
g=9.81; % m/s^2   the acceleration due to gravity

A2=[...
 0,                         0,                                      0, 1, 0, 0; ...
 0,                         0,                                      0, 0, 1, 0; ...
 0,                         0,                                      0, 0, 0, 1; ...
 0,          -(g*(M1 + M2))/M,                                      0, 0, 0, 0; ...
 0,  (g*(M + M1 + M2))/(L1*M),                        -(M2*g)/(L1*M1), 0, 0, 0; ...
 0, -(g*(M + M1 + M2))/(L1*M), (g*(L1*M1 + L1*M2 + L2*M2))/(L1*L2*M1), 0, 0, 0 ...
];

B2=[...
        0;...
        0;...
        0;...
      1/M;...
-1/(L1*M);...
 1/(L1*M) ...
 ];

%% Controllability
disp('========== Controllability ==========')

F1=rand(flip(size(B1)))*mean(mean(A1));
F2=rand(flip(size(B2)))*mean(mean(A2));

eigA1=eig(A1);
eigA1=sort(eigA1,'ComparisonMethod','real');
eigA1B1F1=eig(A1+B1*F1);
eigA1B1F1=sort(eigA1B1F1,'ComparisonMethod','real');

eigA2=eig(A2);
eigA2=sort(eigA2,'ComparisonMethod','real');
eigA2B2F2=eig(A2+B2*F2);
eigA2B2F2=sort(eigA2B2F2,'ComparisonMethod','real');

% 3.1.1 3.1.2 Eigen values of A matrix and closed loop
disp('---eig(A1), eig(A1+B1*F1)---')
disp(eigA1)
disp(eigA1B1F1)

disp('---eig(A2), eig(A2+B2*F2)---')
disp(eigA2)
disp(eigA2B2F2)

% 3.1.2 Check condition
% The intersection are in fact empty.
disp('--- Intersections ---')
if isempty(intersect(eigA1, eigA1B1F1)); disp("Empty intersection of eig(A1) and eig(A1B1F1)"); end
if isempty(intersect(eigA2, eigA2B2F2)); disp("Empty intersection of eig(A2) and eig(A2B2F2)"); end

% 3.1.3
% If the insersection of eigen values of matrix A and eigen values of
% matrix (A+BF) is empty we could find the control for this system to
% to make is stable.

Omega1=B1;
for i=1:(size(A1,1)-1)
    Omega1=[B1, A1*Omega1];
end                  % own implementation of [B AB A^B .. A^(n-1)B]
Omega2=ctrb(A2, B2); % Matlab function that computes [B AB A^B .. A^(n-1)B]

disp('--- Conclusions ---')
if abs(det(Omega1'*Omega1)) > 0
    disp('(A1,B1) is controllable')
else
    disp('(A1,B1) is uncontrollable')
end
if abs(det(Omega2'*Omega2)) > 0
    disp('(A2,B2) is controllable')
else
    disp('(A2,B2) is uncontrollable')
end

% rank Ωi < n ⇔ spec(Ai) ∩ spec(Ai + BiF) ̸= ∅ is false
% rank Ωi =n⇔∃F such that spec(Ai)∩spec(Ai +BiF)=∅ is true

% What the conclusion can be drawn concerning the relationship between rank Ω and the asymptotic stabilisation of the system (3) by the state feedback (4)?
% We are not able to conclude that statement.

% Would it be possible to stabilise the statelite using the linear state feedback (4)?
% NO

% simulation study of the feedback systems 1 and 2 for various F
figure(1)
A=A1;
x0=rand(size(A,1),1);
[t,x]=ode45(@(t,x)A*x, [0,10], x0);
plot(t,x), title('system 1, zero input signal')

figure(2)
A=A2;
x0=rand(size(A,1),1);
[t,x]=ode45(@(t,x)A*x, [0,10], x0);
plot(t,x), title('system 2, zero input signal')

figure(3)
A=A1+B1*F1;
x0=rand(size(A,1),1);
[t,x]=ode45(@(t,x)A*x, [0,10], x0);
plot(t,x), title('system 1, u=F1*x, F1=random')

figure(4)
A=A2+B2*F2;
x0=rand(size(A,1),1);
[t,x]=ode45(@(t,x)A*x, [0,10], x0);
plot(t,x) , title('system 2, u=F2*x, F2=random')

% Additional
F = -place(A2, B2, [-7.5+0.3*i,-7.5-0.3*i,-6.5+0.9*i,-6.5-0.9*i,-3.3+2.3i,-3.3-2.3i]);
figure(5)
A=A2+B2*F;
x0=rand(size(A,1),1);
[t,x]=ode45(@(t,x)A*x, [0,10], x0);
plot(t,x) , title('system 2, u=F2*x, F2=stable')

%% Change of Coordinates

disp('========== Change of Coordinates ==========')

T1=rand(size(A1))*mean(mean(A1));
A1t=T1*A1*inv(T1);
T2=rand(size(A2))*mean(mean(A2));
A2t=T2*A2*inv(T2);

eigA1t=eig(A1t);
eigA2t=eig(A2t);
eigA1t=sort(eigA1t,'ComparisonMethod','real');
eigA2t=sort(eigA2t,'ComparisonMethod','real');

disp('---eig(A1), eig(T1*A1*inv(T1))----')
disp(eigA1)
disp(eigA1t)
disp('---eig(A2), eig(T2*A2*inv(T2)----')
disp(eigA2)
disp(eigA2t)

% Present and briefly discuss the results of the step 3 in the above procedure.
%
% The eigenvalues for the pole placement are the same before and after
% change in coordinates. The values for the satellite are different.
% This differences are from the fact that the satellite is uncontrollable.
% Therefore there is no transformation that can diagonalize the A matrix
% resulting in lack of possibility to fully control the system.

disp('========== Controller Canonical Form ==========')
vT=[zeros(1,size(Omega2,1)-1),1]*inv(Omega2);
T=vT;
for i=1:size(A2,1)-1
    T=[T;vT*A2^i];
end
A2tc=T*A2*inv(T);
B2tc=T*B2;
disp('--- T*A2*inv(T) ---')
disp(A2tc)
disp('--- T*B2 ---')
disp(B2tc)

disp('--- the last row of -A2tc (flipped) and characteristic polynomial of A2 ---')
poly1=[1,flip(-A2tc(end,:))];
poly2=charpoly(A2tc);
disp(poly1)
disp(poly2)

% Explain the advantages of the controller canonical form from the view
% point of the pole placement controller design?
%
% We could construct the A matrix simply from designated eigenvalues. In
% other words when constructing the characteristic polynomial from desired
% eigenvalues we obtain the last row of the matrix A. This is really simply
% way of constructing it.

%% Ackermann's Formula

disp('========== Ackermann Formula ==========')
desiredClosedLoopEigenvalues=...
    [-7.5+0.3*i,-7.5-0.3*i,-6.5+0.9*i,-6.5-0.9*i,-3.3+2.3i,-3.3-2.3i];
desiredCLCharPoly=poly(desiredClosedLoopEigenvalues);
desiredCLCharPolyCoeff=flip(desiredCLCharPoly);
desiredCLCharPolyCoeff(end)=[];
Ft=-A2tc(end,:)-desiredCLCharPolyCoeff;
F=Ft*T;
disp('--- A2tc+B2tc*Ft ---')
A2tcB2tcFt=A2tc+B2tc*Ft;
disp(A2tcB2tcFt)

A2B2F=A2+B2*F;

chp1=charpoly(A2tcB2tcFt);
chp2=charpoly(A2B2F);

disp('--- the desired closed loop characteristic polynomial ---')
disp(desiredCLCharPoly)
disp('--- the characteristic polynomial of A2tc+B2tc*Ft---')
disp(chp1)
disp('--- the characteristic polynomial of A2+B2*F---')
disp(chp2)

% simulation study of the feedback systems 1 and 2 for various F
x0=rand(size(A2,1),1);
x0t=T*x0;

figure(6)
A=A2tcB2tcFt;
[t,x]=ode45(@(t,x)A*x, [0,10], x0t);
plot(t,x), title('system 2 (contr. can. form), u=F*x,pole placement control')

figure(7)
A=A2B2F;
[t,x]=ode45(@(t,x)A*x, [0,10], x0);
plot(t,x), title('system 2, u=F*x,pole placement control')
