# Control Theory Lab

## Authors and acknowledgment

Jakub Kozłowicz 2023  
This repository contains matlab scripts related to Control Theory course at
Wroclaw University of Science and Technology. Majority of this scripts have
been provided by tutor.

## License

MIT License 2023
