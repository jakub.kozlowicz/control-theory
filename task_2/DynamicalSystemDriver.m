%% DYNAMICALSYSTEMDRIVER - a script for testing DynamicalSystem class 

%% help
help DynamicalSystem
help DynamicalSystem.trajectory
help DynamicalSystem.dsplots

%% preceding activities
clear all
close all

% auxiliary variables
f = 0; % figures counter
tf = 100; % cut-off time

% linear dynamical system parameters
name = "demo";
A = [0, -1; 1, -2];
B = [3; 1];
C = [0, 1];
D = [0];

% handles to selected input functions
sigm = 0.005;
impulse = @(t)exp(-t.^2/(2*sigm^2))/(sigm*sqrt(2*pi)); % gaussian function
omega = 0.1;
rectangular = @(t)sign(sin(omega*t)); 
zero = @(t)0;


%% TASK 1: define a handle to the step function
step = @(t)ones(size(t)).*(t>0);

%% creating of a DynamicalSystem class object named ds with the following
% parameters: name, A, B, C, D, zero
ds = DynamicalSystem(name, A, B, C, D, zero);

%% ds system trajectory (evolution of y, x, u over time) for x0=[10; 0];
% together with u for x0=[10;0] in a separate figure No f.
x0 = [10; 0];
figure("Name", "System trajectory (evolution of y, x, u over time)");
ds.dsplots(tf, x0);

%% Plot the zero response of ds over the time interval [0,tf] for
% x0=[0;0] (time plots of y,x,u) in a separate figure No f.
x0 = [0; 0];
figure("Name", "Response of ds for a zero [0, tf]");
ds.dsplots(tf, x0);

%% ds system impulse response
% TASK2: plot the impulse response of ds over the time interval [0,tf] for
% x0=[0;0] (time plots of y,x,u) in a separate figure No f.
x0 = [0; 0];
figure("Name", "Response of ds for an impulse");
ds.u = impulse;
ds.dsplots(tf, x0);

%% ds system step response
% TASK 3: plot the step response of ds over the time interval [0,tf] for
% x0=[0;0] (time plots of y,x,u) in a separate figure No f.
x0 = [0; 0];
figure("Name", "Response of ds for a step");
ds.u = step;
ds.dsplots(tf, x0);

%% ds system response for a rectangular input function
% TASK 4: plot the response of ds for a rectangular in time input function
% over the time interval [0,tf] for x0=[0;0] (time plots of y,x,u) 
% in a separate figure No f.
x0 = [0; 0];
figure("Name", "Response of ds for a rectangular");
ds.u = rectangular;
ds.dsplots(tf, x0);

% TASK 5: plot the response of ds for a rectangular in time input function
% over the time interval [0,tf] for x0=[0;0] (time plots of y,x,u) 
% in a separate figure No f. This time the parameter omega in the
% definition of the handle to rectangular has to be larger than in TASK 4 
omega = 0.7;
rectangular = @(t)sign(sin(omega*t)); 
x0 = [0; 0];
figure("Name", "Response of ds for a rectangular, increased omega");
ds.u = rectangular;
ds.dsplots(tf, x0);

%% ds system state trajectory in a state space
% TASK 6: plot the state trajectory in the state space obtained as 
% the response of ds for a rectangular in time input function
% over the time interval [0,tf] for x0=[0;0] in a separate figure No f. 
x0 = [0; 0];
figure("Name", "System state trajectory in a state space");
ds.u = rectangular;
[x, y, u, t] = ds.trajectory(tf, x0);

m = size(ds.A, 1);
if m == 3
    plot3(x(:,1), x(:,2), x(:,3));
    grid on, xlabel('x_1'), ylabel('x_2'), zlabel('x_3');
elseif m == 2
    plot(x(:, 1)',x(:, 2)');
    grid on, xlabel('x_1'), ylabel('x_2');
end
