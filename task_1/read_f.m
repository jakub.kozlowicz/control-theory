function read_f(filename)
% READ_F Reads dynamical systems from given file and saves to gloal
% database.
    global database;
    system_counter = 0; % Intentionally set to 0, to increment in loop
    [fid, ~] = fopen(filename, "r");
    while ~feof(fid)
        line = fgetl(fid);
        if contains(line, "system") || contains(line, "RLC")
           system_counter = system_counter + 1;
           database(system_counter).name = line;
           database(system_counter).A = eval(fgetl(fid));
           database(system_counter).B = eval(fgetl(fid));
           database(system_counter).C = eval(fgetl(fid));
           database(system_counter).D = eval(fgetl(fid));
        end
    end
    fclose(fid);
end