% DATABASE - a class for data base for dynamical systems
% DATABASE.READ_F - loading a database from a file to the workspace
% DATABASE.READ_K - adding a new element of a database using a keyboard
% DATABASE.SAVE_DB - saving the contents of a database to an ASCII file
% DATABASE.CONTENTS_DB - lists the contents of a database
% DATABASE.SEARCH_DB - search for a string in a database
% DATABASE.STABLE_S - lists all the asymptotically stable systems in a database
% DATABASE.PLOTS - displays plots of the output and the state of a system in DB
% DYNAMICALSYSTEM - a class representing a dynamical system
% DYNAMICALSYSTEM.DSPLOTS - displays plots of the output and the state o a dynamical system
% DYNAMICALSYSTEM.TRAJECTORY - returns x, y, u, t