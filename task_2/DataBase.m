classdef DataBase < handle
    % DataBase - this class represents a data base for linear, time
    % invariant SISO dynamical systems in the state space form

    properties
        DataFileName = "noFile.txt";
        Data = DynamicalSystem;
        count = 0;
    end

    methods
        function obj = DataBase(dbfilename)
            % DataBase - this method construct an instance of this class
            if nargin == 1
                obj.DataFileName = dbfilename;
                [fid, message] = fopen(obj.DataFileName,'r');
                if fid == -1
                    disp(message);
                else
                    obj.count = 0;
                    while feof(fid) == 0
                     name = fgetl(fid);
                     A = eval(fgetl(fid));
                     B = eval(fgetl(fid));
                     C = eval(fgetl(fid));
                     D = eval(fgetl(fid));
                     system = DynamicalSystem(name, A, B, C, D, ...
                         @(t)exp(-t.^2/(2*0.1^2))/(0.1*sqrt(2*pi)));
                     obj.count = obj.count + 1;
                     obj.Data(obj.count) = system;
                    end
                st = fclose(fid);
                end
            end
        end

        function search_db(obj, sub_name)
            % SEARCH_DB(string) - this method lists on a screen (with paging)
            % the names of all the systems in a database, that include
            % the string string of characters.
            if obj.count > 0
                more(5);
                for counter = 1:obj.count
                    name = obj.Data(counter).name;
                    if contains(name, sub_name)
                        fprintf("%s\n", name);
                    end
                end
                more off;
            else
                fprintf('the data base is empty\n');
            end
            
        end

        function sort_N(obj,matrix)
        % SORT_N('matrix') - a method, which lists on a screen (with 
        % paging) the names of the systems from a database in the order 
        % of growing values of 
        % ||matrix||_2 , where matrix is included in {'A', 'B', 'C', 'D'}.
            if obj.count > 0
                for ind = 1:obj.count
                    normM(ind) = norm(eval(['obj.Data(ind).', matrix]) ,2);  
                end
                while any(normM >= 0)
                    aux = find(normM == max(normM));
                    fprintf('%s\t%f\n', obj.Data(aux(1)).name, normM(aux(1)));
                    normM(aux(1)) = -1;
                end
            else
                fprintf('the data base is empty\n');
            end
        end
        
        function stable_s(obj)
            % STABLE_S - this method lists on a screen (with paging) 
            % the names of systems from a database that are asymptotically 
            % stable.
            more(5);
            for counter = 1:obj.count
                is_stable = all(real(eig(obj.Data(counter).A)) < 0);
                if is_stable
                    fprintf("%s\n", obj.Data(counter).name);
                end
            end
            more off;
        end

        function plots(obj, name, tf)
            % PLOTS plots output trajectories (as time functions) and
            % state space trajectories (provided it is feasible) for
            % systems in the data base of which names include the string
            % name
            if obj.count > 0
                indf = 0;
                for ind = 1:obj.count
                    if ~isempty(strmatch(name, obj.Data(ind).name))
                        indf = indf + 1;
                        figure(indf);
                        n = size(obj.Data(ind).A, 1);
                        x0 = zeros(n, 1);
                        [x, y, u, t] = obj.Data(ind).trajectory(tf, x0);   
                        subplot(211);
                        plot(t,y);
                        grid on, xlabel('t'), ylabel('y');
                        title(obj.Data(ind).name);
                        m = size(obj.Data(ind).A, 1);
                        if m == 3
                            subplot(212);
                            plot3(x(:,1), x(:,2), x(:,3));
                            %plot3(x(:, 1)', x(:, 1)', x(:, 1)');
                            grid on, xlabel('x_1'), ylabel('x_2'), zlabel('x_3');
                            title("State Space Trajectory");
                        else
                            if m == 2
                            subplot(212);
                            plot(x(:, 1)',x(:, 2)');
                            grid on, xlabel('x_1'), ylabel('x_2');
                            else
                                if m == 1
                                    subplot(212);
                                    text(0.5, 0.5, 'dim x<2');
                                else
                                    subplot(212);
                                    text(0.5, 0.5, 'dim x>3');
                                end
                            end
                        end
                    end
                end
            end
        end

        function save_db(obj, filename)
            % SAVE_DB(filename) - a method, which saves the contents 
            % of a database to the ASCII file filename
            [fid, ~] = fopen(filename, "w");
            for counter = 1:obj.count
                fprintf(fid, "%s\n", obj.Data(counter).name);
                fprintf(fid, "%s\n", mat2str(obj.Data(counter).A));
                fprintf(fid, "%s\n", mat2str(obj.Data(counter).B));
                fprintf(fid, "%s\n", mat2str(obj.Data(counter).C));
                fprintf(fid, "%s\n", mat2str(obj.Data(counter).D));
            end
            fclose(fid);
        end 

        function contents_db(obj)
            % CONTENTS_DB - a method, which lists on a screen (with 
            % paging) the names of all the dynamical systems stored in 
            % a database
            more(5);
            for counter = 1:obj.count
                fprintf("%s\n", obj.Data(counter).name);
            end
            more off;
        end

        function read_k(obj)
            % READ_K - a method, which allows to add a new dynamical 
            % system to a database using a keyboard
            counter = obj.count + 1;
            system_name = input("Enter the name of the dynamical system", "s");
            obj.Data(counter).name = system_name;
            matrix_a = input("Enter A matrix in form [1 2; ...]", "s");
            obj.Data(counter).A = eval(matrix_a);
            matrix_b = input("Enter B matrix in form [1 2; ...]", "s");
            obj.Data(counter).B = eval(matrix_b);
            matrix_c = input("Enter C matrix in form [1 2; ...]", "s");
            obj.Data(counter).C = eval(matrix_c);
            matrix_d = input("Enter D matrix in form [1 2; ...]", "s");
            obj.Data(counter).D = eval(matrix_d);
        end
        
        function read_f(obj,filename)
            % READ_F(filename) - a method, which reads parameters of 
            % dynamical systems from the ASCII file filename and then 
            % assigns them to suitable fields of the variable database
            obj.DataFileName = filename;
            [fid, message] = fopen(obj.DataFileName, 'r');
            if fid == -1
                disp(message);
            else
                while feof(fid) == 0
                    obj.count = obj.count + 1;
                    obj.Data(obj.count).name = fgetl(fid);
                    obj.Data(obj.count).A = eval(fgetl(fid));
                    obj.Data(obj.count).B = eval(fgetl(fid));
                    obj.Data(obj.count).C = eval(fgetl(fid));
                    obj.Data(obj.count).D = eval(fgetl(fid));
                    obj.Data(obj.count).u = @(t)exp(-t.^2/(2*0.1^2))/(0.1*sqrt(2*pi));
                end
            st = fclose(fid);
            end
        end
    end
end
