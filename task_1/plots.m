function plots(name, tf)
% PLOTS Plots response of the dynamical system specified by argument name
% for given time. Also plots state space tarjectory.
    global database;
    db_size = length(database);
    for counter = 1:db_size
        system_name = database(counter).name;
        if strcmp(name, system_name)
            system_index = counter;
            break;
        end
    end

    global A B;
    A = database(system_index).A;
    B = database(system_index).B;
    dim_a = size(A, 1);
    subplot(2, 1, 1);
    xlabel("t");
    ylabel("y");
    [t, y] = ode45(@dynamics, [0 tf], zeros(1, dim_a));
    plot(t, y);
    grid on;
    title(name);

    subplot(2, 1, 2);
    if dim_a > 3
        text(0.5, 0.5, "dim_a > 3");
    elseif dim_a < 2
        text(0.5, 0.5, "dim_a < 2");
    elseif dim_a == 3
        plot3(y(:,1), y(:,2), y(:,3));
        xlabel('x_1');
        ylabel('x_2');
        zlabel('x_3');
    elseif dim_a == 2
        plot(y(:,1), y(:,2));
        xlabel('x_1');
        ylabel('x_2');
    end
    grid on;
    title("State Space Trajectory");
end