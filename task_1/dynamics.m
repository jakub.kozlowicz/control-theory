function xdot = dynamics(t, x)
% DYNAMICS Represents basic dynamic system.
    global A B;
    xdot = A * x + B * impulse_i(t); 
end