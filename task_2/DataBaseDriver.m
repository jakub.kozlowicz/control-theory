%%% DATABASEDRIVEER - a script for testing a DataBase class
% (solution)

%% task_2 toolbox documentation
help task_2

%% class documentation
disp('++++++++++++ class documentation ++++++++++++++++')
help DataBase

disp('++++++++++++ class methods documentation ++++++++')
help DataBase.read_f
help DataBase.read_k
help DataBase.save_db
help DataBase.contents_db
help DataBase.search_db
help DataBase.sort_N
help DataBase.stable_s
help DataBase.plots

%% preceding activities
clear all
close all

% addpath(pwd);

%% creating the DataBase class object db with the following
% parameter: database_z.txt

db=DataBase('database_z.txt');

%% basic operations on the db object
disp("++++++++++++ db1 contents_db ++++++++++++++++++++++++")
db.contents_db()
disp("++++++++++++ db1 search_db ++++++++++++++++++++++++++")
db.search_db('system');
disp("++++++++++++ db1 sort_N +++++++++++++++++++++++++++++")
db.sort_N('B');
disp("++++++++++++ db1 stable_s +++++++++++++++++++++++++++")
db.stable_s()
disp("++++++++++++ db1 plots (the word system in the name)+")
db.plots("system",10);
disp("++++++++++++ db1 read_k +++++++++++++++++++++++++++++")
db.read_k()
disp("++++++++++++ db1 plots (all plots)+++++++++++++++++++")
db.plots("",10);
%% saving the data stored in db on the file database_zz.txt
disp("++++++++++++ db1 save_db ++++++++++++++++++++++++++++")
db.save_db('database_x.txt')

%% creating the DataBase class object db2 with the default parameters

db2=DataBase;

%% basic operations on the db object
disp("++++++++++++ db2 contents_db ++++++++++++++++++++++++")
db2.contents_db()
disp("++++++++++++ db2 search_db ++++++++++++++++++++++++++")
db2.search_db('system');
disp("++++++++++++ db2 sort_N +++++++++++++++++++++++++++++")
db2.sort_N('B');
disp("++++++++++++ db2 stable_s +++++++++++++++++++++++++++")
db2.stable_s()
disp("++++++++++++ db2 plots (all plots) ++++++++++++++++++")
db2.plots("",10);
disp(db2.count)
disp("++++++++++++ db2 read_f +++++++++++++++++++++++++++++")
db2.read_f('database_x.txt');
disp("++++++++++++ db2 contents_db ++++++++++++++++++++++++")
disp(db2.count)
db2.contents_db()
disp("++++++++++++ db2 search_db ++++++++++++++++++++++++++")
db2.search_db('system');
disp("++++++++++++ db2 sort_N +++++++++++++++++++++++++++++")
db2.sort_N('B');
disp("++++++++++++ db2 stable_s +++++++++++++++++++++++++++")
db2.stable_s()
disp("++++++++++++ db2 plots (all plots) ++++++++++++++++++")
db2.plots("",10);
