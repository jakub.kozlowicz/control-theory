%% FEEDBACK LINEARIZATION OF A SINGLE-LINK  FLEXIBLE-JOINT PENDULUM
% Example 6.10 from the book "Applied Nonlinear Control" by Slotine & Li
clear all
close all

%% Mathematical modeling
% constructing (declaring) symbolic variables and symbolic functions
syms t q1(t) q2(t) D2q1 D2q2
syms x1 x2 x3 x4
syms X1(t) X2(t) X3(t) X4(t)
syms z1 z2 z3 z4
syms u1 v
syms M1 L1 I1 % the link No1 parameters (mass, length, inertia)
syms J1       % the roto parameter (inertia)
syms K1       % the spring parameter (spring constant)
syms G        % the gravity constant
syms a1 a2 a3 % coefficients of a linear combination

% defining the parameters of the double pendulum on the cart
m1=10; % kg            the link mass
l1=2;  % m             the length of the link
J=1;   %               rotor inertia
k=1;   % Nm/rad        the spring constant
gravity=9.81; % m/s^2  the acceleration due to gravity
% I1=M1*L1^2

% kinetic and potential energy for a single link flexible joint
% pendulum
PE=-M1*G*L1*cos(q1)+sym(1)/2*K1*(q1-q2)^2;
KE=sym(1)/2*(M1*L1^2)*diff(q1,t)^2+sym(1)/2*J1*diff(q2,t)^2;

% Lagrange function for a double pendulum on a cart
L=KE-PE;

% a single link flexible joint pendulum dynamics in terms
% of Euler-Lagrange equations

% a rotor
D12  = diff(L,diff(q2(t),t));
D22  = diff(L,q2);
eqn2 = diff(D12,t) - D22 == u1;
% a link
D11  = diff(L,diff(q1(t),t));
D21  = diff(L,q1);
eqn1 = diff(D11,t) - D21 == 0;

% transforming the dynamics equations into state space equations

% step 1: q1, q2, q1', q2' are substituted by ...
% x1, x2, x3, x4 in the equations EQN1, EQN2.
% q1'' and q2'' are substituted by D2q1, D2q2
% respectively. The last substitution is technical in nature for
% the purpose of the solve function, used below.
E1 = subs(eqn1, ...
    {diff(q1,t,t), diff(q2,t,t), ...
     diff(q1,t),   diff(q2,t), ...
     q1(t),         q2(t)},...
    {D2q1, D2q2, x2, x4, x1, x3});

E2 = subs(eqn2, ...
    {diff(q1,t,t), diff(q2,t,t), ...
     diff(q1,t),   diff(q2,t), ...
     q1(t),         q2(t)},...
    {D2q1, D2q2, x2, x4, x1, x3});

% step 2: expressing q1'', q2'', represented by D2q1, D2q2,
% by the state variables x1, x2, x3, x4
[F1, F2]=solve([E1, E2], [D2q1, D2q2]);

% step 3: simplifying the expressions obtained in the step 2
%
F1=simplify(F1);
F2=simplify(F2);

% step 4 (a single link flexible joint pendulum dynamics)
% x'=f(x)+g(x)u
% derivation of f, g and h:
f=simplify(subs([x2; F1; x4; F2], {u1}, {0}));
g=simplify(subs([x2; F1; x4; F2], {u1}, {1})-f);

% TASK 1:
% compare the resulted vector fields f and g with their values in the book
% by Slotine & Lee (see page 243)

% The results are the same

%% Analysis of the model
% Test the existence of a linearizing state feedback.

% To that aim one has to derive the set of the following vectorfields
% adfg=[ad_f^0g, ad_f^1g, ad_f^2g, ad_f^3g]
% (they are collected in the form of a matrix) and then test whether they
% are linearly independent and involutive
x=[x1,x2,x3,x4];
adfg=g;
for i=1:length(x)-1
    adfg=[adfg, jacobian(adfg(1:4,i), x)*f-jacobian(f, x)*adfg(1:4,i)];
end

% TASK 2:
% compare the resulted vector fields with their values in the book
% by Slotine & Lee (see page 243)

% The results are the same

% linearity of [ad_f^0g, ad_f^1g, ad_f^2g, ad_f^3g]
if det(adfg) == 0
    disp('[ad_f^0g, ad_f^1g, ad_f^2g, ad_f^3g] are linearly dependent')
else
    disp('[ad_f^0g, ad_f^1g, ad_f^2g, ad_f^3g] are linearly independent')
end
%
% involutivity of [ad_f^0g, ad_f^1g, ad_f^2g]
isinvol=true;
for i=1:3
    for j=i:3
        aux=jacobian(adfg(1:4,i), x)*adfg(1:4,j)- ...
            jacobian(adfg(1:4,j), x)*adfg(1:4,i);
        invol(i,j)=solve(aux==a1*adfg(1:4,1)+a2*adfg(1:4,2)+a3*adfg(1:4,3),a1,a2,a3);
        if ~all(isreal([invol(i,j).a1, invol(i,j).a2, invol(1,1).a3]))
            disp('[ad_f^0g, ad_f^1g, ad_f^2g, ad_f^3g] are not involutive')
            isinvol=false;
        end
    end
end
if isinvol;
    disp('[ad_f^0g, ad_f^1g, ad_f^2g, ad_f^3g] seems to be involutive')
end

% TASK 3:
% compare the results obtained here with the results reported in the book
% by Slotine & Lee (see page 243)

% The results are the same. The system is input-state linearizable.

%% State transformation

% coordinate transformation
T.z1=x1;
T.z2=transpose(gradient(T.z1,x))*f;
T.z3=transpose(gradient(T.z2,x))*f;
T.z4=transpose(gradient(T.z3,x))*f;
Tt=subs(T,{x1,x2,x3,x4},{X1(t),X2(t),X3(t),X4(t)});


% inverse coordinate transformation
invT=solve(T.z1==z1, T.z2==z2, T.z3==z3, T.z4==z4, x1, x2, x3, x4);

% TASK 4:
% compare the results obtained here with the results reported in the book
% by Slotine & Lee (see the pages 244 and 245)

% The results are the same.

%% Linearizing state feedback
% u=alpha(x)+beta(x) v,
% where beta=1/(L_g L_f^3 T.z1) and alpha=-(L_f^4 T.z1)/(L_g L_f^3 T.z1)

beta =                              simplify(1/(transpose(gradient(T.z4,x))*g));
alpha= simplify(-transpose(gradient(T.z4,x))*f/(transpose(gradient(T.z4,x))*g));
u=alpha+beta*v;

% 1) Apply the linearizing feedback to the system x'=f(x)+g(x)*u
% Dx below denotes a time derivative of x
Dx=simplify(subs(simplify(f+g*u), {x1,x2,x3,x4}, {invT.x1, invT.x2, invT.x3, invT.x4}));

% 2) Apply the state transformation to the system x'=f(x)+g(x)*(alpha(x)+beta(x)*v)
% Dz below denotes a time derivative of z
Dz1=simplify(subs(diff(Tt.z1,t), {diff(X1(t),t), diff(X2(t),t), diff(X3(t),t),diff(X4(t),t), X1(t), X2(t), X3(t), X4(t)},{Dx(1), Dx(2),Dx(3), Dx(4), invT.x1, invT.x2, invT.x3, invT.x4}));
Dz2=simplify(subs(diff(Tt.z2,t), {diff(X1(t),t), diff(X2(t),t), diff(X3(t),t),diff(X4(t),t), X1(t), X2(t), X3(t), X4(t)},{Dx(1), Dx(2),Dx(3), Dx(4), invT.x1, invT.x2, invT.x3, invT.x4}));
Dz3=simplify(subs(diff(Tt.z3,t), {diff(X1(t),t), diff(X2(t),t), diff(X3(t),t),diff(X4(t),t), X1(t), X2(t), X3(t), X4(t)},{Dx(1), Dx(2),Dx(3), Dx(4), invT.x1, invT.x2, invT.x3, invT.x4}));
Dz4=simplify(subs(diff(Tt.z4,t), {diff(X1(t),t), diff(X2(t),t), diff(X3(t),t),diff(X4(t),t), X1(t), X2(t), X3(t), X4(t)},{Dx(1), Dx(2),Dx(3), Dx(4), invT.x1, invT.x2, invT.x3, invT.x4}));
Dz=[Dz1; Dz2; Dz3; Dz4];

% TASK 5:
% Argue that the form of Dz allows to conclude that the dynamics of a
% single-link flexible-joint manipulator after application of the
% linearizing state feedback and the state transformation is as follows:
% z1'=z2, z2'=z3, z3'=z4, z4'=v
% compare the results obtained here with the results reported in the book
% by Slotine & Lee (see the pages 244)

% The results are the same.

%% Control law for the linear system where z is a state and v is an input
% stablization at z=0
z=[z1;z2;z3;z4];
A=double(jacobian(Dz,z));
B=double(jacobian(Dz,v));
closedLoopEigenValues=[-1,-1,-1,-1];
F=-acker(A,B,closedLoopEigenValues);
vz=F*z; % a stabilizing control law

% TASK 6:
% compare the procedure applied here with the contents of the section
% CONTROLLER DESIGN BASED ON INPUT-STATE LINEARIZATION in the book
% by Slotine & Lee (see the pages 245 and 246)

% The procedure is the same.

%% Control law for the nonlinear system where x is a state and u is an input

% 1) transformation of variables from z to x in the control law v
vx=simplify(subs(vz, {z1,z2,z3,z4}, {T.z1, T.z2, T.z3, T.z4}));
% 2) substitution of vx to the control law u
ucl=simplify(subs(u,v,vx));

%% A closed-loop system
% The control objective is stabilization of the system at x=0
% fcl - a closed-loop system vector field
fcl=simplify(f+g*ucl);

%% Simulation
x0=[pi;-10;-pi;10];
%x0=[0;0;0;0];
z0=[x0(1);
    x0(2);
   -(k*x0(1) - k*x0(3) + gravity*l1*m1*sin(x0(1)))/(l1^2*m1);
    (k*x0(4))/(l1^2*m1) - (x0(2)*(k + gravity*l1*m1*cos(x0(1))))/(l1^2*m1)];

tf=100;

options = odeset('RelTol',1e-10,'AbsTol',1e-12);
model1=@(t,x)(A+B*F)*x;

% open-loop system: x'=f(x)+g(x)*0
sf=subs(f, {M1, L1, J1, K1, G},{m1,l1,J, k,gravity});
model2 = matlabFunction(sf,'Vars',{t,[x1;x2;x3;x4]});

% closed-loop system with a stabilizing controller based on feedback
% linearization
sfcl=subs(fcl, {M1, L1, J1, K1, G},{m1,l1,J, k,gravity});
model3 = matlabFunction(sfcl,'Vars',{t,[x1;x2;x3;x4]});

[t,state]=ode45(model1,[0,tf], z0, options);
figure(1)
plot(t,state)
xlabel('t'), ylabel('x')
title('stabilization of the linearized system with z state')

[t,state]=ode45(model2,[0,tf],x0, options);
figure(2)
plot(t,state)
xlabel('t'), ylabel('x')
title('no stabilizing control (u=0)')

[t,state]=ode45(model3,[0,tf],x0, options);
figure(3)
plot(t,state)
xlabel('t'), ylabel('x')
title('stabilization based on feedback linearization')

% TASK 7
% Compare Figure 1 and Figure 2. What conclusion can be drawn about the
% settling time?
% The second figure shows that system does not settle down.

% Compare Figure 2 and Figure 3. What can be said about the stability of
% the equlibrium point x0=0 in the both cases?