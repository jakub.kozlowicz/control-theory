function contents_db()
% CONTENTS_DB Lists names of the systems in the database on screen with
% paging.
    global database;
    more(5);
    for counter = 1:length(database)
        fprintf("%s\n", database(counter).name);
    end
    more off;
end