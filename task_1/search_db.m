function search_db(sub_name)
% SEARCH_DB Searchs the global database for given sub name of the systems
% and prints their nominal name to the screen with paging.
    global database;
    more(5);
    for counter = 1:length(database)
        name = database(counter).name;
        if contains(name, sub_name)
            fprintf("%s\n", name);
        end
    end
    more off;
end