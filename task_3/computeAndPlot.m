function computeAndPlot(figure_name, sD,sLD, x0, x0L, T)

[ts,ys] = ode45(sD,[0 T],x0);
[tls,yls] = ode45(sLD,[0 T],x0L);
figure('Name',figure_name)

% Compare both models on one plot
subplot(4,1,1); hold on; grid on;
plot(ts,ys(:,1))
plot(tls,yls(:,1))
title("Models comparison r(t)")
xlabel('t[s]')
ylabel('r')
legend('Nonlinear', 'Linear','Location', 'best');

subplot(4,1,2); hold on; grid on;
plot(ts,ys(:,2))
plot(tls,yls(:,2))
title("Models comparison r_dot(t)")
xlabel('t[s]')
ylabel('r_dot')
legend('Nonlinear', 'Linear','Location', 'best');

subplot(4,1,3); hold on; grid on;
plot(ts,ys(:,3))
plot(tls,yls(:,3))
title("Models comparison phi(t)")
xlabel('t[s]')
ylabel('phi')
legend('Nonlinear', 'Linear','Location', 'best');

subplot(4,1,4); hold on; grid on;
plot(ts,ys(:,4))
plot(tls,yls(:,4))
title("Models comparison phi_dot(t)")
xlabel('t[s]')
ylabel('phi_dot')
legend('Nonlinear', 'Linear','Location', 'best');

end

