%% A.6 STABILISTION OF A DOUBLE PENDULUM ON A CART
clear all
close all

%% A.6.1 Mathematical modeling
% constructing (declaring) symbolic variables and symbolic functions
syms t q(t) th1(t) th2(t) D2q D2th1 D2th2 x1 x2 x3 x4 x5 x6 u M M1 M2 L1 L2 g

% defining the parameters of the double pendulum on the cart
m=100; % kg            the cart mass
m1=10; % kg            the first link mass
m2=10; % kg            the second link mass
l1=2;  % m             the length of the first link
l2=1;  % m             the length of the second link
gravity=9.81; % m/s^2  the acceleration due to gravity

% kinetic and potential energy for a double pendulum on a cart
PE=M1*g*L1*cos(th1)+M2*g*(L1*cos(th1)+L2*cos(th1+th2));
KE=sym(1)/2*M*diff(q,t)^2+...
    sym(1)/2*M1*( ...
    (diff(q,t)+L1*diff(th1,t)*cos(th1))^2 ...
    + (L1*diff(th1,t)*sin(th1))^2 ) + ...
    sym(1)/2*M2*( ...
    (diff(q,t)+L1*diff(th1,t)*cos(th1) ...
    + L2*(diff(th1,t)+diff(th2,t))*cos(th1+th2))^2 ... 
    + (L1*diff(th1,t)*sin(th1)...
    + L2*(diff(th1,t)+diff(th2,t))*sin(th1+th2))^2 );

% Lagrange function for a double pendulum on a cart
L=KE-PE;

% a double pendulum on a cart dynamics in terms of Euler-Lagrange equations
D11  = diff(L,diff(q(t),t));
D21  = diff(L,q);
eqn1 = diff(D11,t) - D21 == u;

D12  = diff(L,diff(th1(t),t));
D22  = diff(L,th1);
eqn2 = diff(D12,t) - D22 == 0;

D13  = diff(L,diff(th2(t),t));
D23  = diff(L,th2);
eqn3 = diff(D13,t) - D23 == 0;

% transforming the dynamics equations into state space equations
%
% step 1: q, th1, th2, q', th1', th2' are substituted by ... 
% x1, x2, x3, x4, x5, x6 in the equations EQN1, EQN3, EQN2. 
% q'', th1'' and th2'' are substituted by D2r, D2th1, D2th2 
% respectively. The last substitution is technical in nature for 
% the purpose of the solve function, used below.
E1 = subs(eqn1, ...
    {diff(q,t,t), diff(th1,t,t), diff(th2,t,t), ...
     diff(q,t),   diff(th1,t),   diff(th2,t), ...
     q(t),        th1(t),         th2(t)},...
    {D2q, D2th1, D2th2, x4, x5, x6, x1, x2, x3});

E2 = subs(eqn2, ...
    {diff(q,t,t), diff(th1,t,t), diff(th2,t,t), ...
     diff(q,t),   diff(th1,t),   diff(th2,t), ...
     q(t),        th1(t),         th2(t)},...
    {D2q, D2th1, D2th2, x4, x5, x6, x1, x2, x3});

E3 = subs(eqn3, ...
    {diff(q,t,t), diff(th1,t,t), diff(th2,t,t), ...
     diff(q,t),   diff(th1,t),   diff(th2,t), ...
     q(t),        th1(t),         th2(t)},...
    {D2q, D2th1, D2th2, x4, x5, x6, x1, x2, x3});

% step 2: expressing q'', th1'', th2'', represented by D2q, D2th1, D2th2, 
% by the state variables x1, x2, x3, x4, x5, x6
[F1, F2, F3]=solve([E1, E2, E3], [D2q, D2th1, D2th2]);

% step 3: simplifying the expressions obtained in the step 2
%
F1=simplify(F1);
F2=simplify(F2);
F3=simplify(F3);

% step 4 (a pendulum on a cart dynamics)
% x'=f(x,u)
% y =h(x,u)
% derivation of f and h:
f=[x4; x5; x6; F1; F2; F3];
h=[x1;L1*sin(x2); x1+L1*sin(x2)+L2*sin(x2+x3)];

%% A.6.2 Linearization
% verification of selected solutions of the double pendulum on the cart equations of motion

% x(t)=[0;0;0;0;0;0];, u(t)=0;
test1=simplify(diff([0;0;0;0;0;0],t)...
    -subs(f,{x1,x2,x3,x4,x5,x6,u},...
    {0,0,0,0,0,0,0}));
if all(test1==0) 
    disp('x(t)=[0;0;0;0;0;0] is a solution of the double pendulum on the cart state space equations');
end

% computation of the A, B, C matrices in the linear approximation of
% x'=f(x,u)
% y =h(x,u)
% at the equilibrium

% step 1: derivation of Jacobians
JA=jacobian(f,[x1,x2,x3,x4,x5,x6]);
JB=jacobian(f,u);
JC=jacobian(h,[x1,x2,x3,x4,x5,x6]);

% step 2: derivation of the symbolic matrices A, B, C that are associated
% with the equilibrium point [0,0,0,0,0,0]'
SA=simplify(subs(JA,{x1,x2,x3,x4,x5,x6,u},{0,0,0,0,0,0,0}));
SB=simplify(subs(JB,{x1,x2,x3,x4,x5,x6,u},{0,0,0,0,0,0,0}));
SC=simplify(subs(JC,{x1,x2,x3,x4,x5,x6},{0,0,0,0,0,0}));

% step 3: derivation of the numerical matrices A, B, C by substituting the
% the double pendulum on the cart parameters under corresponding symbolic
% variables in the symbolic matrices obtained in step 2.
A=double(simplify(subs(SA, {M,M1,M2,L1,L2,g},{m,m1,m2,l1,l2,gravity})));
B=double(simplify(subs(SB, {M,M1,M2,L1,L2,g},{m,m1,m2,l1,l2,gravity})));
C=double(simplify(subs(SC, {M,M1,M2,L1,L2,g},{m,m1,m2,l1,l2,gravity})));

%
% step 4:
% x'=A*x + B*u
% linear approximation of f(x,u) by fl(x,u):=A*x + B*u
fl=A*[x1;x2;x3;x4;x5;x6]+B*u;

%% A.6.3 Analysis of the model
% stability of the linearized system
disp('A.6.3')

disp('stability testing:')
nA=size(A,1);
ev=eig(A);
if all(real(ev)<0) disp("the linearized system (A,B) is asymptotically stable"); 
else
    if any(real(ev)>0) disp("the linearized system (A,B) is unstable (re eig(A) >0)");
    else
        stability=true;
        for i=1:length(ev)
            evm(i,1)=ev(i);
            evm(i,2)=sum(ev==ev(i));
            evm(i,3)=rank(null(eye(nA)*ev(i)-A));
            if all([real(evm(i,1))==0, evm(i,2)~=evm(i,3)])
                stability=false;
            end
        end
        if stability 
            disp("the linearized system (A,B) is stable"); 
        else
            disp("the linearized system (A,B) is unstable");
        end
    end
end

disp('controllability testing:')
if rank(ctrb(A,B)) == size(A,1) disp('the linearized system (A,B) is controllable'); end;
if rank(ctrb(A,B))  < size(A,1) disp('the linearized system (A,B) is not controllable'); end;

%% A.6.4 Simulation
% simulation study of the double pendulum on the cart dynamics

T=10; % the simulation end time

% definition of the feedback control signal stabilising 
% the double pendulum on the cart at xe=0
% u=K*x

P=[-7.5+0.3i, -7.5-0.3i, -6.5+0.9*i,...
    -6.5-0.9*i, -3.3+2.3i, -3.3-2.3i]; % desired closed-loop poles
K=-acker(A,B,P); % feedback gain (pole placement, Ackermann's formula)
Su=K*[x1;x2;x3;x4;x5;x6]; % a control law in a symbolic form
fcl=subs(f,{u},{Su}); % fcl(x)=f(x, K*x)
fcl=simplify(fcl);
flcl=subs(fl,{u},{Su}); % flcl=A*x+B*K*x
flcl=simplify(flcl);

% 0
% the system state in the equilibrium point

% the initial condition
x0=[0;0;0;0;0;0];

% double pendulum on cart dynamics (dpcD) creation
dpcDf=subs(fcl,{M,M1,M2,L1,L2,g},{m,m1,m2,l1,l2,gravity});
dpcD = matlabFunction(dpcDf,'Vars',{t,[x1;x2;x3;x4;x5;x6]});
% linearized double pendulum on cart dynamics (dpcLD) creation
dpcLDfl=subs(flcl,{M,M1,M2,L1,L2,g},{m,m1,m2,l1,l2,gravity});
dpcLD = matlabFunction(dpcLDfl,'Vars',{t,[x1;x2;x3;x4;x5;x6]});

% double pendulum on cart dynamics (dpcD) simulation and plotting
[ts,ys] = ode45(dpcD,[0 T],x0);
figure(1)
subplot(2,1,1)
plot(ts,ys)
title("nonlinear model")
xlabel("t[s]"), ylabel("x")
% linearized double pendulum on cart dynamics (dpcLD) simulation and plotting
[ts,ys] = ode45(dpcLD,[0 T],x0);
subplot(2,1,2)
plot(ts,ys)
title("linearized model")
xlabel("t[s]"), ylabel("x")


% 1
% the system state in the neighbourhood of the equilibrium point

x0=[-0.5;0;0;0;0;0];
% double pendulum on cart dynamics (dpcD) simulation and plotting
[ts,ys] = ode45(dpcD,[0 T],x0);
figure(2)
subplot(2,1,1)
plot(ts,ys)
title("nonlinear model")
xlabel("t[s]"), ylabel("x")
% linearized double pendulum on cart dynamics (dpcLD) simulation and plotting
[ts,ys] = ode45(dpcLD,[0 T],x0);
subplot(2,1,2)
plot(ts,ys)
title("linearized model")
xlabel("t[s]"), ylabel("x")
