%% A.4 SATELLITE DYNAMICS
clear all
close all

%% A.4.2 Mathematical modeling
% constructing (declaring) symbolic variables and symbolic functions
syms t r(t) th(t) phi(t) D2r D2phi x1 x2 x3 x4 ur uth dr dth R OMEGA DELTA M K

% defining the satellite parameters
T=24*60*60;           % s, period of the earth turnover
m=2;                  % kg
k=4e14;               % m^3/s^2
omega=2*pi/T;         % rad/s
rR=(k/omega^2)^(1/3); % m, the radius of the satellite's orbit

% kinetic and potential energy for the satellite
PE=-(K*M)/r;
KE=sym(1)/2*M*(diff(r,t)^2 + r^2*diff(th,t)^2);

% Lagrange function for the satellite
L=KE-PE;

% satellite dynamics in terms of Euler-Lagrange equations
D11  = diff(L,diff(r(t),t));
D21  = diff(L,r);
eqn1 = diff(D11,t) - D21 == ur+dr;
% expression of the variable th using the variable phi by the substitution 
% operation
EQN1 = subs(eqn1, th(t), phi(t)+OMEGA*t);

D12  = diff(L,diff(th(t),t));
D22  = diff(L,th);
eqn2 = diff(D12,t) - D22 == uth+dth;
% expression of the variable th using the variable phi by the substitution 
% operation
EQN2 = subs(eqn2, th(t), phi(t)+OMEGA*t);

% transforming the satellite dynamics equations into state space equations
%
% step 1: r, r', phi, phi' are substituted by x1, x2, x3, x4 in the
% equations EQN1 and EQN2. r'' and phi '' are substituted by D2r and D2phi
% respectively. The last substitution is technical in nature for 
% the purpose of the solve function, used below.
E1 = subs(EQN1, ...
    {diff(r,t,t), diff(phi,t,t), diff(r,t), diff(phi,t), r(t), phi(t)},...
    {D2r, D2phi, x2, x4, x1, x3});

E2 = subs(EQN2, ...
    {diff(r,t,t), diff(phi,t,t), diff(r,t), diff(phi,t), r(t), phi(t)},...
    {D2r, D2phi, x2, x4, x1, x3});

% step 2: expressing r'', phi'', represented by D2r and D2phi, by the state
% variables x1, x2, x3, x4
[F1, F2]=solve(E1, E2, D2r, D2phi);

% step 3: simplifying the expressions obtained in the step 2
%
F1=simplify(F1);
F2=simplify(F2);

% step 4 (satellite dynamics)
% x'=f(x,u)
% y =h(x,u)
% derivation of f and h:
f=[x2;F1;x4;F2];
h=[x1;x3];

%% A.4.3 Equilibrium Analysis
% verification of selected solutions of the equation of satellite dynamics

disp('A.4.3')

% x(t)=[R;0;0;0];
test1=simplify(diff([R;0;0;0],t)...
    -subs(f,{x1,x2,x3,x4,ur,dr,uth,dth,K},...
    {R,0,0,0,0,0,0,0,OMEGA^2*R^3}));
if all(test1==0) 
    disp('x(t)=[R;0;0;0] is a solution of the satellite state space equations');
end

% x(t)=[(K/(OMEGA+DELTA)^2)^(1/3);0;DELTA*t;diff(DELTA*t,t)];
test2=simplify(diff([(K/(OMEGA+DELTA)^2)^(1/3);0;DELTA*t;diff(DELTA*t,t)],t)...
    -subs(f,{x1,x2,x3,x4,ur,dr,uth,dth},...
    {(K/(OMEGA+DELTA)^2)^(1/3),0,DELTA*t,diff(DELTA*t,t),0,0,0,0}));
if all(test2==0) 
    disp('x(t)=[(K/(OMEGA+DELTA)^2)^(1/3);0;DELTA*t;diff(DELTA*t,t)] is a solution of the satellite state space equations');
end

%% A.4.4 Linearization
% Computation of the A, B, C matrices in the linear approximation of
% x'=f(x,u)
% y =h(x,u)
% at the equilibrium

% Step 1: derivation of Jacobians
JA=jacobian(f,[x1,x2,x3,x4]);
JB=jacobian(f,[ur,uth,dr,dth]);
JC=jacobian(h,[x1,x2,x3,x4]);

% Step 2: derivation of the symbolic matrices A, B, C that are associated
% with the equilibrium point [R,0,0,0]' taking into account that 
% K=OMEGA^2*R^3
SA=simplify(subs(JA,{x1,x2,x3,x4,ur,dr,uth,dth,K},{R,0,0,0,0,0,0,0,OMEGA^2*R^3}));
SB=simplify(subs(JB,{x1,x2,x3,x4,ur,dr,uth,dth},{R,0,0,0,0,0,0,0}));
SC=simplify(subs(JC,{x1,x2,x3,x4},{0,0,0,0}));

% Step 3: derivation of the numerical matrices A, B, C by substituting the
% satellite parameters under corresponding symbolic variables in the
% symbolic matrices obtained in step 2.
A=double(simplify(subs(SA, {K,M,OMEGA,R},{k,m,omega, rR})));
B=double(simplify(subs(SB, {K,M,OMEGA,R},{k,m,omega, rR})));
C=double(simplify(subs(SC, {K,M,OMEGA,R},{k,m,omega, rR})));

% Step 4:
% x'=A*x + B*u
% linear approximation of f(x,u) by fl(x,u):=A*x + B*u
fl=A*[x1;x2;x3;x4]+B*[ur;uth;dr;dth];

%% A.4.5 Analysis of the model
% Stability of the linearized system

disp('A.4.5')
disp('stability testing:')
ev=eig(A);
disp(ev);

%% A.4.6 Simulation
% simulation study of the satellite dynamics

% definition of a common input signals used during simulation study 
% of the satellite dynamics
u0=@(t)0;

% 0. System in the equilibrium point

% The initial conditions and the radial disturbance
dr0=@(t)0;
x0=[rR;0;0;0];
x0L=x0-[rR;0;0;0];

% code generation for satellite dynamics (sD)
sDf=subs(f,{K,M,OMEGA,R,dr,dth,ur,uth},{k,m,omega,rR,dr0,u0,u0,u0});
sD=matlabFunction(sDf,'Vars',{t,[x1;x2;x3;x4]});
% code generation for linearized satellite dynamics (sLD)  
sLDfl=subs(fl,{dr,dth,ur,uth},{dr0,u0,u0,u0});
sLD=matlabFunction(sLDfl,'Vars',{t,[x1;x2;x3;x4]});

computeAndPlot("System at the equilibrium point", sD, sLD, x0, x0L, T);

% 1. Initial disturbance for r(0)

% The initial conditions (the radial disturbance remains unchanged)
x0=[rR*1.01;0;0;0];
x0L=x0-[rR;0;0;0];

computeAndPlot("Initial disturbance for r(0)", sD, sLD, x0, x0L, T);

% 1. Initial disturbance for phi(0)

% The initial conditions (the radial disturbance remains unchanged)
x0=[rR;2*pi/100;0;0];
x0L=x0-[rR;0;0;0];

computeAndPlot("Initial disturbance for phi(0)", sD, sLD, x0, x0L, T);

% 2. Radial step disturbance input equal to
% 5% of the gravitational pull of the earth

% the initial conditions and the radial disturbance
dr1=@(t)(sign(t)+1)*m*9.81/2*0.05;
x0=[rR;0;0;0];
x0L=x0-[rR;0;0;0];

% Code generation for satellite dynamics (sD)
sDf=subs(f,{K,M,OMEGA,R,dr,dth,ur,uth},{k,m,omega,rR,dr1,u0,u0,u0});
sD=matlabFunction(sDf,'Vars',{t,[x1;x2;x3;x4]});

% Code generation for linearized satellite dynamics (sLD)  
sLDfl=subs(fl,{dr,dth,ur,uth},{dr1,u0,u0,u0});
sLD=matlabFunction(sLDfl,'Vars',{t,[x1;x2;x3;x4]});

computeAndPlot("Radial step disturbance", sD, sLD, x0, x0L, T);

% 3. Radial periodic disturbance input with the amplitude equal to 5% of 
% the gravitational pull of the earth with period equal to the period of
% revolution of the earth

% The radial disturbance (the initial conditions remain unchanged)
x0=[rR;0;0;0];
x0L=x0-[rR;0;0;0];
dr2=@(t)cos(2*pi/T*t)*m*9.81*0.05;

% Code generation for satellite dynamics (sD)
sDf=subs(f,{K,M,OMEGA,R,dr,dth,ur,uth},{k,m,omega,rR,dr2,u0,u0,u0});
sD=matlabFunction(sDf,'Vars',{t,[x1;x2;x3;x4]});

% Code generation for linearized satellite dynamics (sLD)  
sLDfl=subs(fl,{dr,dth,ur,uth},{dr2,u0,u0,u0});
sLD=matlabFunction(sLDfl,'Vars',{t,[x1;x2;x3;x4]});

computeAndPlot("Radial periodic disturbance", sD, sLD, x0, x0L, T);
