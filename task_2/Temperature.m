%% TEMPERATURE - a script for simulating temterature box

%% preceding activities
clear all
close all

% auxiliary variables
f = 0; % figures counter
tf = 100; % cut-off time

a1 = 0.5;
a2 = 1;
a3 = 0.1;
a4 = 0.2;
b1 = 0.1;
y1 = 0.7;
y2 = 0.05;

% linear dynamical system parameters
name = "Temperature container";
A = [-a1, 0, 0; a2, -a2-a3, a3; 0, a4, -a4];
B1 = [b1; 0; 0];
B2 = [a1; 0; 0];
B = [B1, B2];
C = [0, -y2, y1];
D = [0];

% handles to selected input functions
sigm = 0.005;
impulse = @(t)exp(-t.^2/(2*sigm^2))/(sigm*sqrt(2*pi)); % gaussian function
omega = 0.1;
rectangular = @(t)sign(sin(omega*t)); 
zero = @(t)zeros(size(t));
step = @(t)ones(size(t)).*(t>0);

ds = DynamicalSystem(name, A, B, C, D, zero);

%% Task 1 - zero for all
x0 = [1; 0; 0];
ds.u = @(t)[zero(t); zero(t)];
figure("Name", "Zero for all");
ds.dsplotsM(tf, x0);

%% Task 2 - zero for u and Ta step
x0 = [1; 0; 0];
ds.u = @(t)[zero(t); step(t)];
figure("Name", "Zero for u and Ta step");
ds.dsplotsM(tf, x0);

%% Task 3 - use different Kp for something
KP = [1, 2, 8, -5, -10];
x0 = [1; 0; 0];

figure("Name", "Response of Tc for different Kp values");
for i = 1:length(KP)
    ds.A = A + B1 * KP(i) * C;
    ds.u = @(t)[zero(t); zero(t)];
    [x, h, u, t] = ds.trajectory(tf, x0);
    plot(t, x(:,1));
    grid on, hold on, xlabel("t"), ylabel("Tc"), title("Tc for different Kp");
end
for i = 1:length(KP)
    legend_labels{i} = sprintf('Kp=%d', KP(i));
end
legend(legend_labels);

%% Task 4 - Response of Tc for step Ta
KP = [1, 2, 8, -5, -10];
x0 = [1; 0; 0];

figure("Name", "Zero for u and Ta step");
for i = 1:length(KP)
    ds.A = A + B1 * KP(i) * C;
    ds.u = @(t)[zero(t); step(t)];
    [x, h, u, t] = ds.trajectory(tf, x0);
    plot(t, x(:,1));
    grid on, hold on, xlabel("t"), ylabel("Tc"), title("Tc for different Kp step");
end
for i = 1:length(KP)
    legend_labels{i} = sprintf('Kp=%d', KP(i));
end
legend(legend_labels);

%% Task 5 - System with control with closed loop
KP = [-10, -10];
KI = [-1, 1];
x0 = [1; 0; 0; 0];

figure("Name", "System with control with closed loop");
ds.B = [[b1; 0; 0; 0],[a1; 0; 0; 0]];
ds.C = [C, 0];
ds.u = @(t)[zero(t); step(t)];
for i = 1:length(KP)
    ds.A = [A, zeros(3,1); C, 0] + [B1; 0]*[KP(i)*C, KI(i)];
    [x, h, u, t] = ds.trajectory(tf,x0);
    plot(t,x(:,1));
    grid on, hold on, xlabel("t"), ylabel("Tc"), title("Closed loop control");
end

for i = 1:length(KP)
    legend_labels{i} = sprintf('Kp=%d Ki=%d', KP(i), KI(i));
end
legend(legend_labels);