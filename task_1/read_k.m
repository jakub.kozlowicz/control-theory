function read_k()
% READ_K Reads dynamical system from the keyboard asking user for input.
    global database;
    len = length(database);
    counter = len + 1;
    system_name = input("Enter the name of the dynamical system", "s");
    database(counter).name = system_name;
    matrix_a = input("Enter A matrix in form [1 2; ...]h", "s");
    database(counter).A = eval(matrix_a);
    matrix_b = input("Enter B matrix in form [1 2; ...]", "s");
    database(counter).B = eval(matrix_b);
    matrix_c = input("Enter C matrix in form [1 2; ...]", "s");
    database(counter).C = eval(matrix_c);
    matrix_d = input("Enter D matrix in form [1 2; ...]", "s");
    database(counter).D = eval(matrix_d);
end