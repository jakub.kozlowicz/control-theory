function y = impulse_i(t)
% IMPULSE_I Sumilates impulse input function.
    sigma = 0.00005;
    y = (1 / sqrt(2 * sigma^2 * pi()) * exp(-(t^2)/(2 * sigma^2)));
end