function stable_s()
% STABE_S Prints to the screen with paging all stable systems from the
% database.
    global database;
    more(5);
    db_size = length(database);
    for counter = 1:db_size
        is_stable = all(real(eig(database(counter).A)) < 0);
        if is_stable
            fprintf("%s\n", database(counter).name);
        end
    end
    more off;
end